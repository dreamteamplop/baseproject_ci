<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public function __construct() {
        parent::__construct();

        $this->load->add_package_path(APPPATH.'third_party/debugbar');
        $this->load->library('console');
        $this->output->enable_profiler(TRUE);

        $this->console->debug('Hello world !');
    }
}


require(APPPATH.'/third_party/restserver/core/Restserver_controller.php');