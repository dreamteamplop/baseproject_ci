#!/usr/bin/env bash

composer_install(){
	
	# Test if Composer is installed
	composer -v > /dev/null 2>&1
	COMPOSER_IS_INSTALLED=$?
	if [[ $COMPOSER_IS_INSTALLED -ne 0 ]]; then
	    echo "Composer is required, you can get it using : sudo apt-get install composer";
	    return;
	fi
	  
	doInstal=true
	
	echo --------------------
	echo : COMPOSER INSTALL :
	echo --------------------
	
	# make sure we're in the right directory
	if [ ! -f composer.json ]; then
		echo "File composer.json not found"
		doInstal=false
	fi
	if [ ! -f bower.json ]; then
		echo "File bower.json not found"
		doInstal=false
	fi

	# make sur composer components not alreay exists
	if [ -f application/third_party/debugbar/config/profiler.php ]; then
		echo "Profiler component already exists"
		doInstal=false
	fi
	if [ -f application/third_party/bower/config/bower.php ]; then
		echo "Bower component already exists"
		doInstal=false
	fi
	if [ -f application/third_party/restserver/config/restserver.php ]; then
		echo "Restserver component already exists"
		doInstal=false
	fi
	if [ -f application/third_party/origami/config/origami.php ]; then
		echo "Origami component already exists"
		doInstal=false
	fi
	
	if [ $doInstal = true ]; then
	
		composer install
		
		# make sur configuration files not already exists
		if [ ! -f application/config/profiler.php ]; then
			cp application/third_party/debugbar/config/profiler.php application/config/
		fi
		if [ ! -f application/config/bower.php ]; then
			cp application/third_party/bower/config/bower.php application/config/
		fi
		if [ ! -f application/config/restserver.php ]; then
			cp application/third_party/restserver/config/restserver.php application/config/
		fi
		if [ ! -f application/config/origami.php ]; then
			cp application/third_party/origami/config/origami.php application/config/
		fi
	fi
}

bower_install() {
	
	# Test if Composer is installed
	bower -v > /dev/null 2>&1
	BOWER_IS_INSTALLED=$?
	if [[ $BOWER_IS_INSTALLED -ne 0 ]]; then
	    echo "Bower is required";
	    return;
	fi

	echo --------------------
	echo : BOWER INSTALL :
	echo --------------------
	
	bower install
}

git_relocate(){

	read -p "New origin URL : " yn
	case $yn in
	    http* ) 
		git remote add origin $yn
		git push --set-upstream origin master
		break;;
	    * ) echo "Relocate aborted - this is not a valid URL";;
	esac
}


# Test if PHP is installed
php -v > /dev/null 2>&1
PHP_IS_INSTALLED=$?

[[ $PHP_IS_INSTALLED -ne 0 ]] && { printf "PHP is required"; return; }

echo "Checking PHP version :";
php -v | grep "PHP 5";
echo ""

echo "A) Install composer dependencies ?"
select yn in "Yes" "No"; do
      case $yn in
	  Yes ) composer_install; break;;
	  No ) break;;
      esac
done

echo "B) Install bower dependencies ?"
select yn in "Yes" "No"; do
      case $yn in
	  Yes ) bower_install; break;;
	  No ) break;;
      esac
done

# removes the remote GIT origin and its references from your local repository
echo "The GIT remote origin will be unlinked to not push in the baseproject_CI which must stay a starter project"
git remote rm origin

echo "C) Relocate to a new GIT remote origin ?"
select yn in "Yes" "No"; do
      case $yn in
	  Yes ) git_relocate; break;;
	  No ) break;;
      esac
done



echo ---------------------
echo : INSTALLATION DONE :
echo ---------------------

echo "You can now delete this installer"


